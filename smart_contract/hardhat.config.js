require("@nomiclabs/hardhat-waffle");

module.exports = {
  solidity: "0.8.0",
  networks: {
    ropsten: {
      url: "https://eth-ropsten.alchemyapi.io/v2/NF7vmqt0l-1gudUGU6Dco1ggTZjHX0Sb",
      accounts: [
        "68e507e4b9691faf420c7b38b275e815985c5ef60758459ad94e78b773ec4759",
      ],
    },
  },
};
